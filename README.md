# File Replication #

An algorithm for optimal file replication using dynamic programming for the course Design and Analysis of Algorithms (Master) at FCUL.

If you're interested, take a look at the report. Lots of work went into writing it. I received full points and finished best of the course :)

The original exercise is from "Algorithm Design" by Jon Kleinberg and Éva Tardos.