package com.johann_hofmann.daa1;

/**
 * A simple Statistic model for an algorithm calculation,
 * containing data about it's computation steps and time, as well as the final server configuration.
 */
public class Stat {
    public String servers;
    public int cost;
    public int steps;
    public long nanoseconds;

    /**
     * Constructs a new Stat
     *
     * @param servers server configuration
     * @param cost the calculated optimal cost
     * @param steps computational steps required
     */
    public Stat(final String servers, final int cost, final int steps, final long nanoseconds) {
        this.servers = servers;
        this.cost = cost;
        this.steps = steps;
        this.nanoseconds = nanoseconds;
    }

    @Override
    public String toString() {
        return "Stat{" +
                "servers='" + servers + '\'' +
                ", cost=" + cost +
                ", steps=" + steps +
                ", nanoseconds=" + nanoseconds +
                '}';
    }
}


