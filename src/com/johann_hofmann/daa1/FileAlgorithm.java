package com.johann_hofmann.daa1;

/**
 * The main algorithm implementations.
 * This class implements {@link java.lang.Runnable} and can be run in threads to improve performance.
 */
public class FileAlgorithm implements Runnable {

    private final String method;
    private StatHelper statHelper;
    private final int[] servers;
    private final int number;

    /**
     * Constructs a new FileAlgorithm that uses the specified method to find
     * the minimum file configuration over a set of n servers.
     *
     * @param method     "iteratively", "memoized" or "recursive"
     * @param servers    an array specifying the access cost of each server
     * @param statHelper a {@link StatHelper} to record statistics
     */
    public FileAlgorithm(final String method, final int[] servers, final StatHelper statHelper) {
        this.method = method;
        this.statHelper = statHelper;
        this.servers = servers;
        this.number = servers.length - 1;
    }

    /**
     * Accumulate access costs between nodes x and y.
     *
     * @param x last node that has a file
     * @param y first node that has a file
     * @return accumulated cost
     */
    private int acc(final int x, final int y) {
        final int len = y - x - 1;
        return ((len * len + len) / 2);
    }

    /**
     * Solves the problem by traversing the recurrence from the bottom up
     *
     * @param n number of nodes
     * @return the optimal placement cost for n nodes
     */
    public int bottomUp(final int n) {
        if (n > servers.length) {
            throw new IllegalArgumentException("n can not be higher than the number of servers");
        }

        final int[] opt = new int[n + 1];
        opt[0] = 0; // starting node a

        for (int i = 1; i <= n; i++) {
            int min = Integer.MAX_VALUE;
            int optindex = 0;
            for (int j = 0; j < i; j++) {
                final int res = opt[j] + acc(j, i);
                if (res < min) {
                    min = res;
                    optindex = j;
                }

                if (statHelper != null) {
                    statHelper.step();
                }
            }

            opt[i] = servers[i] + min;
            if (statHelper != null) {
                statHelper.place(optindex, i, opt[i]);
            }
        }
        return opt[n - 1];
    }

    /**
     * Solves the problem by traversing the recurrence recursively
     *
     * @param n    number of nodes
     * @param memo the array to cache calculated values in, or null, if you want the uncached version
     * @return the optimal placement cost for n nodes
     */
    public int recursive(final int n, final int[] memo) {
        if (n > servers.length) {
            throw new IllegalArgumentException("n can not be higher than the number of servers");
        }

        if (n == 0) {
            return 0; // starting node a
        }

        if (memo != null && memo[n] != 0) {
            return memo[n];
        }

        int min = Integer.MAX_VALUE;

        int optindex = 0;
        for (int i = 0; i < n; i++) {
            final int res = recursive(i, memo) + acc(i, n);
            if (res < min) {
                min = res;
                optindex = i;
            }

            if (statHelper != null) {
                statHelper.step();
            }
        }

        final int val = servers[n] + min;

        if (statHelper != null) {
            statHelper.place(optindex, n, val);
        }

        if (memo != null) {
            memo[n] = val;
        }

        return val;
    }

    @Override
    public void run() {
        switch (method) {
            case "iterative":
                statHelper.start();
                bottomUp(number);
                break;
            case "recursive":
                final StatHelper tempHelper = statHelper;
                statHelper = new StatHelper(method);
                statHelper.start();
                tempHelper.start();
                for (int i = 0; i <= number; i++) {
                    tempHelper.place(0, i, recursive(i, null));
                    tempHelper.getStats().get(i).servers = statHelper.getStats().get(i).servers;
                    tempHelper.setCount(statHelper.getCount());
                    statHelper.setCount(0);
                }
                break;
            case "memoized":
                statHelper.start();
                recursive(number, new int[number + 1]);
                break;
        }
    }
}
