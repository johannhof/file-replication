package com.johann_hofmann.daa1;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages Stats and can be used by {@link FileAlgorithm}
 */
public class StatHelper {

    private final String method;
    private Map<Integer, Stat> stats;
    private int count;
    private long startTime;

    /**
     * Constructs a new StatHelper
     *
     * @param method iterative, recursive or memoized
     */
    public StatHelper(final String method) {
        this.method = method;
        this.count = 0;
        this.stats = new HashMap<>();
        stats.put(0, new Stat("0", 0, 0, 0));
    }

    /**
     * Counts one calculation step. (For asymptotic complexity)
     */
    public void step() {
        this.count++;
    }

    /**
     * Start timer
     */
    public void start(){
        startTime = System.nanoTime();
    }

    /**
     * Place a new calculation stat
     *
     * @param lastMin the last node that gave the minimum result for n
     * @param i the node index that was calculated
     * @param cost the total cost
     */
    public void place(final int lastMin, final int i, final int cost) {
        final String last = stats.get(lastMin).servers;
        final StringBuilder servers = new StringBuilder(last);
        for (int j = 0; j < i - last.length(); j++) {
            servers.append("0");
        }
        servers.append("1");
        stats.put(i, new Stat(servers.toString(), cost, count, System.nanoTime() - startTime));
    }

    public int getCount() {
        return count;
    }

    public void setCount(final int count) {
        this.count = count;
    }

    public String getMethod() {
        return method;
    }

    public Map<Integer, Stat> getStats() {
        return stats;
    }

    public void setStats(final Map<Integer, Stat> stats) {
        this.stats = stats;
    }

    public void printServers() {
        System.out.println(method);
        stats.forEach((i, stat) ->
                System.out.println(i + " : " + stat));
    }
}
