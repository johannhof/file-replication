package com.johann_hofmann.daa1;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

import java.util.Map;

/**
 * Creates wonderful graphs out of provided datasets.
 */
public class Grapher {

    private final DefaultCategoryDataset dataset;
    private final String name;
    private final String xAxis;
    private final String yAxis;

    /**
     * Creates a new Grapher
     */
    public Grapher(final String name, final String xAxis, final String yAxis) {
        this.name = name;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        dataset = new DefaultCategoryDataset();
    }

    /**
     * Add the reference values n and n^2 to the chart
     *
     * @param count number of servers
     */
    public void addReferenceValues(final int count){
        for (Integer i = 0; i < count + 1; i++) {
            dataset.addValue(i * i, "n^2", i);
        }

        for (Integer i = 0; i < count + 1; i++) {
            dataset.addValue(i, "n", i);
        }
    }

    /**
     * Add a set of computation steps to the Grapher
     *
     * @param name name of the set
     * @param set set to display
     */
    public void addSteps(final String name, final Map<Integer, Stat> set) {
        set.forEach((index, stat) -> dataset.addValue(stat.steps, name, index));
    }

    /**
     * Add a set of computation times to the Grapher
     *
     * @param name name of the set
     * @param set set to display
     */
    public void addTime(final String name, final Map<Integer, Stat> set) {
        set.forEach((index, stat) -> dataset.addValue(stat.nanoseconds, name, index));
    }

    /**
     * Creates a new chart
     *
     * @return a new chart containing the data that was added previously
     */
    public JFreeChart createChart() {
        return ChartFactory.createLineChart(name, xAxis, yAxis, dataset);
    }

}
